<!-- Block user information module NAV  -->
{if $is_logged}
	<div class="header_user_info">
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
	</div>
{/if}
<div class="header_user_info">
	{if $is_logged}
		<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
			{l s='Sign out' mod='blockuserinfo'}
		</a>
	{else}
		<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
			{l s='Sign in' mod='blockuserinfo'}
		</a>
                 
                
	{/if}
</div>
        <div class="header_user_info">
            <a class="iframe" href="http://190.98.210.197/clop/vencimiento/">Consultar puntos</a>
        </div>
        <div class="header_user_info">
            <a class="iframe" href="http://localhost:8000/Login">Login</a>
        </div>
{literal}
        <script type="text/javascript">
          $(document).ready(function(){
            $("a.iframe").fancybox({
            'autoScale'  : false,
            'transitionIn'  : 'none',
            'transitionOut'  : 'none',
            'width'  : 500,
            'height'  : 500,
            'type'  : 'iframe'});
            });
        </script>
{/literal}
<!-- /Block usmodule NAV -->
