<?php /*%%SmartyHeaderCode:2363658232a2c6e9ed0-38174015%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '21aca7f3e87ebc891d6d1f6a15def49632394ee5' => 
    array (
      0 => 'C:\\wamp\\www\\prestashop\\themes\\Corpuntos\\modules\\blockcategories\\blockcategories_footer.tpl',
      1 => 1478699414,
      2 => 'file',
    ),
    '3e2f94f68fb7948f5923348537d1db70b2afa03c' => 
    array (
      0 => 'C:\\wamp\\www\\prestashop\\themes\\Corpuntos\\modules\\blockcategories\\category-tree-branch.tpl',
      1 => 1478699414,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2363658232a2c6e9ed0-38174015',
  'variables' => 
  array (
    'isDhtml' => 0,
    'blockCategTree' => 0,
    'child' => 0,
    'numberColumn' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58232a2c82cba4_58569737',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58232a2c82cba4_58569737')) {function content_58232a2c82cba4_58569737($_smarty_tpl) {?>
<!-- Block categories module -->
<section class="blockcategories_footer footer-block col-xs-12 col-sm-2">
	<h4>Categorías</h4>
	<div class="category_footer toggle-footer">
		<div class="list">
			<ul class="tree dhtml">
												
<li class="last">
	<a 
	href="http://dev.prestashop.com/3-mujer" title="Aquí encontrarás todas las colecciones de moda de mujer.  
 Esta categoría incluye todos los básicos de tu armario y mucho más: 
 ¡zapatos, complementos, camisetas estampadas, vestidos muy femeninos y vaqueros para mujer!">
		Mujer
	</a>
			<ul>
												
<li >
	<a 
	href="http://dev.prestashop.com/4-tops" title="Aquí encontrarás camisetas, tops, blusas, camisetas de manga corta, de manga larga, sin mangas, de media manga y mucho más. 
 ¡Encuentra el corte que mejor te quede!">
		Tops
	</a>
			<ul>
												
<li >
	<a 
	href="http://dev.prestashop.com/5-camisetas" title="Los must have de tu armario; ¡échale un vistazo a los diferentes colores, 
 formas y estilos de nuestra colección!">
		Camisetas
	</a>
	</li>

																
<li class="last">
	<a 
	href="http://dev.prestashop.com/7-blusas" title="Combina tus blusas preferidas con los accesorios perfectos para un look deslumbrante.">
		Blusas
	</a>
	</li>

									</ul>
	</li>

																
<li class="last">
	<a 
	href="http://dev.prestashop.com/8-vestidos" title="¡Encuentra tus vestidos favoritos entre nuestra amplia colección de vestidos de noche, vestidos informales y vestidos veraniegos! 
 Te ofrecemos vestidos para todos los días, para cualquier estilo y cualquier ocasión.">
		Vestidos
	</a>
			<ul>
												
<li >
	<a 
	href="http://dev.prestashop.com/9-vestidos-informales" title="¿Estás buscando un vestido para todos los días? Échale un vistazo a 
 nuestra selección para encontrar el vestido perfecto.">
		Vestidos informales
	</a>
	</li>

																
<li >
	<a 
	href="http://dev.prestashop.com/10-vestidos-noche" title="¡Descubre nuestra colección y encuentra el vestido perfecto para una velada inolvidable!">
		Vestidos de noche
	</a>
	</li>

																
<li class="last">
	<a 
	href="http://dev.prestashop.com/11-vestidos-verano" title="Cortos, largos, de seda, estampados... aquí encontrarás el vestido perfecto para el verano.">
		Vestidos de verano
	</a>
	</li>

									</ul>
	</li>

									</ul>
	</li>

							
										</ul>
		</div>
	</div> <!-- .category_footer -->
</section>
<!-- /Block categories module -->
<?php }} ?>
